Platform.sh
===========

This module displays a bar at the bottom of your site with various information about the environment you currently viewing.

We do this by adding a div that is fixed to the bottom of the page. We can read environment variables configured in Platform to find the environment name and then add the value to Drupal's JavaScript settings array for display on the site.
